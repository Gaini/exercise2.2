const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const usersObjects = require('./users.json');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.post("/users", function (req, res) {
    const method = req.body.method;

    /*
     Создание нового пользователя
     {"jsonrpc": "2.0", "method": "create", "name": "name1", "score": 1}
     */
    if (method == 'create') {
        usersObjects[usersObjects.length] = {
            id: usersObjects.length + 1,
            name: req.body.name,
            score: req.body.score
        };
        res.send(usersObjects[usersObjects.length-1]);
    }

    /*
     Получение списка всех пользователей
     {"jsonrpc": "2.0", "method": "read"} весь список
     {"jsonrpc": "2.0", "method": "read", "id": 2} только с id=2
     {"jsonrpc": "2.0", "method": "read", "id": 3, "name": "Greg"} с id=3 и name=Greg
     */
    if (method == 'read') {
        let result = usersObjects.filter(element => element !== null);

        if (typeof req.body.id !== "undefined" ) {
            result = result.filter(element => element.id === req.body.id );
        }
        if (typeof req.body.name !== "undefined" ) {
            result = result.filter(element => element.name === req.body.name );
        }
        if (typeof req.body.score !== "undefined") {
            result = result.filter(element => element.score === req.body.score );
        }

        res.send(result);
    }

    /*
     Редактирование пользователя по ID
     {"jsonrpc": "2.0", "method": "update", "id": "1", "name": "name3", "score": 3}  у пользователя с id=1 будет изменено name на name3 и score=3
     */
    if (method == 'update') {

        if (usersObjects[req.body.id-1] === undefined) {
            res.status(400).send('No such record');
        } else {
            usersObjects[req.body.id-1] = {
                id: req.body.id,
                name: req.body.name,
                score: req.body.score
            };

            res.send(usersObjects[req.body.id-1]);
        }
    }
    /*
     Удаление пользователя по ID
     {"jsonrpc": "2.0", "method": "delete", "id": "1"}
     */
    if (method == 'delete') {
        if (usersObjects[req.body.id-1] === undefined) {
            res.status(400).send('No such record');
        } else {
            delete usersObjects[req.body.id-1];
            res.send('Record - '+req.body.id+' is deleted');
        }
    }

});

app.listen(3000);
