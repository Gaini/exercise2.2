const express = require("express");
const app = express();
const rtAPIv1 = express.Router();
const usersObjects = require('./users.json');
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/*
 Получение списка всех пользователей
 */
rtAPIv1.get("/users", function (req, res) {
    res.send(usersObjects);
});

/*
 Получение пользователя по ID
 */
rtAPIv1.get("/users/:id", function (req, res) {
    var fields = req.query.fields,
        fieldsArray = new Array(),
        currentUser = {};
    if (fields) {
        fields.split(",").forEach(function (element, index, array) {
            currentUser[element] = usersObjects[req.params.id-1][element];
        });
        res.send(currentUser);
    } else {
        res.send(usersObjects[req.params.id-1]);
    }
});

/*
    Создание нового пользователя
    {"name": "Peter", "score": 4}
 */
rtAPIv1.post('/users', (req, res) => {
    usersObjects[usersObjects.length] = {
        id: usersObjects.length+1,
        name: req.body.name,
        score: req.body.score
    };
    res.send(usersObjects[usersObjects.length-1]);

});

/*
 Редактирование пользователя по ID
 {"name": "Peter", "score": 4}
 */
rtAPIv1.put('/users/:id', (req, res) => {
    if (usersObjects[req.params.id-1] === undefined) {
        res.status(400).send('No such record');
    } else {
        usersObjects[req.params.id-1] = {
            id: req.params.id,
            name: req.body.name,
            score: req.body.score
        };
        res.send(usersObjects[req.params.id-1]);
    }
});

/*
 Удаление пользователя по ID
 */
rtAPIv1.delete('/users/:id', (req, res) => {
    if (usersObjects[req.params.id-1] === undefined) {
        res.status(400).send('No such record');
    } else {
        usersObjects.splice(req.params.id-1, 1);
        res.send(usersObjects);
    }
});

app.listen(3000);
app.use("/api/v1", rtAPIv1);

